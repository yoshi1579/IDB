# Overview

[ConnectPetsTo.Me](http://www.connectpetsto.me) is a website that allows visitors to find pets that they might want to adopt, or a shelter where they might like to volunteer.

# [Project's Class Page](https://www.cs.utexas.edu/users/downing/cs373/projects/IDB.html)

# [API Documentation](https://documenter.getpostman.com/view/4670195/RWMFqT2u)

# [Technical Specification](https://gitlab.com/CS373-18SU-GRP/IDB/blob/master/Technical%20Report.pdf)

# [UML (download to view)](https://gitlab.com/CS373-18SU-GRP/IDB/tree/master/uml)

# [Presentation] (https://gitlab.com/CS373-18SU-GRP/IDB/blob/master/ConnectPetsTo.ME.pdf)

# --------------------------------------------------------------------------------

# README: Class requirement section

## Name, EID, and GitLab ID, of all members

    * Abdullah Abualsoud (AJ),    aa73228,    ajipsum
    * Alex Flores Escacega,       af28459,    afloresescarcega
    * David Mao,                  dm46452,    the_color_blurple
    * Jeffrey Zhu,                jwz269,     jeffwzhu
    * William Wesley Monroe,      wwm394,     purpleHey
    * Yoshinobu Nakada,           yn2536,     yoshi1579

## Git SHA

    d23ade7de9e57d757056d93988cd36a97ea4eaac

## Link to website:

[ConnectPetsTo.Me](http://www.connectpetsto.me)

## Estimated completion time (hours: int)

    * Abdullah Abualsoud (AJ) : 25
    * Alex Flores Escacega    : 20
    * David Mao               : 20
    * Jeffrey Zhu             : 20
    * Wesley Monroe           : 20
    * Yoshinobu Nakada        : 25

## Actual completion time (hours: int)

    * Abdullah Abualsoud (AJ) : 30
    * Alex Flores Escacega    : 20
    * David Mao               : 25
    * Jeffrey Zhu             : 20
    * William Wesley Monroe   : 20
    * Yoshinobu Nakada        : 30

## Comments

!!JULY 26th, 2018 UPDATE:
Our Developer group visualizations are located at the bottom of the paragraph of
"About the Site" in the About page. 
The links are also provided here:
[Threats](http://d3.connectpetsto.me/threats)
[Taxonomy](http://d3.connectpetsto.me/taxonomy)
[Locations](http://d3.connectpetsto.me/locations)

In this phase we implemented git workflow. All our work is on branches.
This is our link to our user stories: [Link](https://gitlab.com/CS373-18SU-GRP/IDB/boards?=&label_name%5B%5D=Stories%20-%20Customer)
