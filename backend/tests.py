#!/usr/bin/env python3

"""
Import Statements
"""
import pprint
import unittest

import flask_testing
from mock import Mock, mock, patch

import database
from app import *
from database import *
from Elasticsearch.data_migration_util import *
from Elasticsearch.pet_es import *
from Elasticsearch.query_es import *
from getPetsUtil import getPetsFromPetFinder

"""
Configurations
"""

DEBUG = False
SQL_DATABASE = False
pp = pprint.PrettyPrinter(indent=4, width=80)

"""
Unit Tests
"""


class UnitTests(flask_testing.TestCase):
    """
    Unit Tests: app.py and database.py
    """
    TESTING = True
    SQLALCHEMY_BINDS = None
    render_templates = False

    # Setting up database for testing
    if SQL_DATABASE:
        SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://'
    else:
        SQLALCHEMY_DATABASE_URI = "sqlite://"

    def create_app(self):
        # Default port is 5000
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_BINDS'] = None

        if SQL_DATABASE:
            app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + \
                username + ':' + password + '@' + hostname + '/' + \
                db_name + '?' + options
        else:
            app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'

        # Initializing app
        db.init_app(app)
        ma.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        self.pet_schema = PetSchema(many=True)
        self.pet_qry = Pet.query
        self.vet_schema = VetSchema(many=True)
        self.vet_qry = Vet.query
        self.shelter_schema = ShelterSchema(many=True)
        self.shelter_qry = Shelter.query

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    """
    test for app.py
    """

    def test_create_table_response(self):
        """
        test for create() - which creates tables in the database
        """
        with self.app.test_request_context():
            response = self.client.get('/create_table')
            self.assertStatus(response, 200)

    def test_create_table_function(self):
        """
        test for create(): check the tables
        """
        response = self.client.get('/create_table')
        self.assertEqual(response.status_code, 200)
        self.assertStatus(response, 200)
        # HTTP method OK. Check result: Verify all the tables are created.
        expected_tables = ["pet", "vet", "shelter",
                           "breed", "close_vet_shelter"]
        db_tables = db.metadata.tables.keys()
        if DEBUG:
            pp.pprint(db_tables)
        [self.assertIn(tbl, db.metadata.tables.keys()) for tbl in db_tables]

    def test_insert_date_1(self):
        """
        test for insert(): count
        """
        response = self.client.get('/insert_data')
        self.assertEqual(response.status_code, 200)
        # [NOTE]: Breed is not currently populated
        # count = [Pet.query.count(), Vet.query.count(),
        #            Shelter.query.count(), Breed.query.count()]
        count = [Pet.query.count(), Vet.query.count(),
                 Shelter.query.count()]
        if DEBUG:
            pp.pprint(count)

        # check that some pets, vets and shelters were inserted
        [self.assertGreater(c, 0x00) for c in count]
        self.assertGreater(len(Pet.query.all()), 0)
        self.assertGreater(len(Vet.query.all()), 0)
        self.assertGreater(len(Shelter.query.all()), 0)

    def test_insert_data_3(self):
        """
        test for insert(): response
        """
        with self.app.test_request_context():
            response = self.client.get('/insert_data')
            if DEBUG:
                qry = Pet.query.all()
                out = self.pet_schema.dump(qry).data
                pp.pprint(out)
            self.assertStatus(response, 200)

    def test_insert_data_4(self):
        """
        test for insert(): count exact
        """
        with self.app.test_request_context():
            response = self.client.get('/insert_data')
            if DEBUG:
                qry = Pet.query.all()
                out = self.pet_schema.dump(qry).data
                pp.pprint(out)
            self.assertStatus(response, 200)
            count = {
                'pet': 55,
                'vet': 20,
                'shelter': 12,
                # 'tbl': 120
            }
            models = {
                'pet': self.pet_qry,
                'vet': self.vet_qry,
                'shelter': self.shelter_qry
            }
            if DEBUG:
                ref = [
                    self.pet_qry.filter_by(animal='bird').count(),
                    self.pet_qry.filter_by(animal='dog').count(),
                    self.pet_qry.filter_by(animal='cat').count()
                ]
                pp.pprint(ref)
            [self.assertEqual(count[mod], models[mod].count())
             for mod in models]

    def test_parse_params(self):
        """
        test for parse_params()
        """
        input = {
            'animal': 'dog',
            'state': 'TX'
        }
        expect1 = {'animal': 'dog'}
        expect2 = {'state': 'TX'}
        pet_cond, shel_cond = parse_params(input)
        self.assertEqual(expect1, pet_cond)
        self.assertEqual(expect2, shel_cond)

    """
    test for database.py
    """

    def test_count_total_zip(self):
        """
        test count_total_zip function
            - count number of zips from middlezip.txt
            - return total number of zipcodes
        """
        filename = 'middlezip.txt'
        total_zip = count_total_zip(filename)
        self.assertEqual(total_zip, 3)

    def test_insert_vet(self):
        """
        test insert_vet function
            - make sure vet are inserted into session
        """
        with patch.object(getPetsUtil, 'getVetsByLocation') \
                as mock_getVetsByLocation:
            expected_return = Vet(id="1", state='TX')
            zipcode = 78731
            # mock calling getVetsByLocation!  returns a list of Vets
            mock_getVetsByLocation.return_value = [expected_return]
            insert_vet(zipcode)
            db.session.commit()
            vets = Vet.query.all()
            for v in vets:
                self.assertEqual(expected_return, v)

    def test_insert_shelter(self):
        """
        test insert_shelter function
            - make sure vet are inserted into session
            - make sure the return function returns a set of pets
              to delete (pets that have no shelter.
        """
        with patch.object(getPetsUtil, 'getShelterByIdFromPetFinder') \
                as mock_getShelterByIdFromPetFinder:
            expected_return = Shelter(id="1")
            input_pet = [Pet(id=1)]
            zipcode = 78731
            mock_getShelterByIdFromPetFinder.return_value = expected_return
            deleted_pet = insert_shelter(input_pet)
            db.session.commit()
            shelters = Shelter.query.all()
            for s in shelters:
                self.assertEqual(expected_return, s)
            self.assertEqual(deleted_pet, set())

    def test_insert_close_vet_shelter_bridgeTable(self):
        """
        test insert_close_vet_shelter_bridgeTable
            - a pair close vet and shelter should be inserted
        """
        with patch.object(database, 'db') as mock_v:
            with patch.object(database, 'db') as mock_s:
                mock_v.session.query(Vet).all.return_value = [Vet(id='1')]
                mock_s.session.query(Shelter).filter_by.return_value = [
                    Shelter(id='1')]

    def test_calculate_distance(self):
        """
        test for calculate_distance
            - calculate distance btw two points
        """
        mile = calculate_distance(lat_1=30, log_1=30, lat_2=40, log_2=40)
        self.assertEqual(int(mile), 891)

    """
    Tests for App.py
    """

    def test_num_pages(self):
        """
        test for num_pages()
        test cases:
        pageSize  numIn  numPages
        9          100      12
        9          9        1
        9          10       2
        """
        self.assertEqual(12, num_pages(100))
        self.assertEqual(1, num_pages(9))
        self.assertEqual(2, num_pages(10))

    def test_filter_by(self):
        """
        test for filter_by()
        """
        # NEED TO WORK ON
        pass

    def test_msg_no_content(self):
        """
        test for msg_no_content()
        """
        response = msg_no_content()
        self.assertIn(204, response)

    def test_parse_params(self):
        """
        test for parse_params()
        """
        input = {
            'animal': 'Dog',
            'age': 'Senior',
            'sex': 'M',
            'size': 'L',
            'state': 'TX',
            'city': 'Austin',
            'rating': 5,
            'sortby': 'name',
            'order': 'ascending',
            'invalidfield': 2049871
        }
        expect1 = {'animal': 'Dog', 'age': 'Senior', 'sex': 'M', 'size': 'L'}
        expect2 = {'state': 'TX', 'city': 'Austin'}
        expect3 = {'rating': 5}
        expect4 = {'name': 'ascending'}
        pet_cond, shel_cond, vet_cond, sort_conds = parse_params(input)
        self.assertEqual(expect1, pet_cond)
        self.assertEqual(expect2, shel_cond)
        self.assertEqual(expect3, vet_cond)
        self.assertEqual(expect4, sort_conds)

    """
    test for API endpoints
    """

    def test_root(self):
        """
        test for the root API endpoint
        """
        with self.app.test_request_context():
            response = self.client.get('/')
            self.assertStatus(response, 200)

    """
    Endpoint for Pet
    """

    def test_api_PetPage_no_param_provided(self):
        """
        test for PetPage()

        Default state=TX
        Should return Pet(state=TX) since no params are provided

        endpoint: /api/pet/page/1
        """
        expected = \
            {'objects': [{'age': None,
                          'all_images': None,
                          'animal': 'dog',
                          'breed': None,
                          'description': None,
                          'id': '1',
                          'image_url': None,
                          'name': None,
                          'sex': None,
                          'shelter': '1',
                          'size': None,
                          'vet': None},
                         {'age': None,
                             'all_images': None,
                             'animal': 'dog',
                             'breed': None,
                             'description': None,
                             'id': '2',
                             'image_url': None,
                             'name': None,
                             'sex': None,
                             'shelter': '1',
                             'size': None,
                             'vet': None},
                         {'age': None,
                          'all_images': None,
                          'animal': 'cat',
                          'breed': None,
                          'description': None,
                          'id': '3',
                          'image_url': None,
                          'name': None,
                          'sex': None,
                          'shelter': '1',
                          'size': None,
                          'vet': None},
                         {'age': None,
                          'all_images': None,
                          'animal': 'dog',
                          'breed': None,
                          'description': None,
                          'id': '4',
                          'image_url': None,
                          'name': None,
                          'sex': None,
                          'shelter': '2',
                          'size': None,
                          'vet': None},
                         {'age': None,
                          'all_images': None,
                          'animal': 'dog',
                          'breed': None,
                          'description': None,
                          'id': '5',
                          'image_url': None,
                          'name': None,
                          'sex': None,
                          'shelter': '2',
                          'size': None,
                          'vet': None}],
                'page': 1,
                'total_pages': 1}
        db.session.add(Pet(id=1, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=2, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=3, shelter_id=1, animal='cat'))
        db.session.add(Pet(id=4, shelter_id=2, animal='dog'))
        db.session.add(Pet(id=5, shelter_id=2, animal='dog'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='SC', city='Aiken'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/page/1')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_PetPage_param_city_animal_provided(self):
        """
        test for PetPage()

        Should return only Pet instances (animal=dog & city=Austin)
        Also, default state=TX

        endpoint: /api/pet/page/1?animal=dog&city=Austin
        """
        expected = \
            {'objects': [{'age': None,
                          'all_images': None,
                          'animal': 'dog',
                          'breed': None,
                          'description': None,
                          'id': '1',
                                'image_url': None,
                                'name': None,
                                'sex': None,
                                'shelter': '1',
                                'size': None,
                                'vet': None},
                         {'age': None,
                          'all_images': None,
                          'animal': 'dog',
                          'breed': None,
                          'description': None,
                          'id': '2',
                          'image_url': None,
                          'name': None,
                          'sex': None,
                          'shelter': '1',
                          'size': None,
                          'vet': None}],
                'page': 1,
                'total_pages': 1}
        db.session.add(Pet(id=1, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=2, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=3, shelter_id=1, animal='cat'))
        db.session.add(Pet(id=4, shelter_id=2, animal='dog'))
        db.session.add(Pet(id=5, shelter_id=2, animal='dog'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='TX', city='Dallas'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/page/1?animal=dog&city=Austin')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_PetPage_size_sorting(self):
        """
        test for PetPage()

        if sortby=size&order=ascending is passed to parameter,
        sort items by size S, M, L, XL

        endpoint: /api/pet/page/1?sortby=size&order=ascending
        """
        expected = \
            {'objects': [{'age': None,
                          'all_images': None,
                          'animal': None,
                          'breed': None,
                          'description': None,
                          'id': '1',
                          'image_url': None,
                          'name': None,
                          'sex': None,
                          'shelter': '1',
                          'size': 'S',
                          'vet': None},
                         {'age': None,
                          'all_images': None,
                          'animal': None,
                          'breed': None,
                          'description': None,
                          'id': '2',
                                'image_url': None,
                                'name': None,
                                'sex': None,
                                'shelter': '1',
                                'size': 'M',
                                'vet': None},
                         {'age': None,
                          'all_images': None,
                          'animal': None,
                          'breed': None,
                          'description': None,
                          'id': '3',
                                'image_url': None,
                                'name': None,
                                'sex': None,
                                'shelter': '1',
                                'size': 'L',
                                'vet': None},
                         {'age': None,
                          'all_images': None,
                          'animal': None,
                          'breed': None,
                          'description': None,
                          'id': '4',
                                'image_url': None,
                                'name': None,
                                'sex': None,
                                'shelter': '2',
                                'size': 'XL',
                                'vet': None}],
                'page': 1,
                'total_pages': 1}
        db.session.add(Pet(id=1, shelter_id=1, size='S'))
        db.session.add(Pet(id=2, shelter_id=1, size='M'))
        db.session.add(Pet(id=3, shelter_id=1, size='L'))
        db.session.add(Pet(id=4, shelter_id=2, size='XL'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='TX', city='Dallas'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()

        # print('test for sort')
        response = self.client.get(
            '/api/pet/page/1?sortby=size&order=ascending')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    """
    Endpoint for Pet
    """

    def test_api_VetPage_rating_sorting(self):
        """
        test for VetPage()

        if rating sort is passed to parameter,
        sort items by rating

        endpoint: /api/vet/page/1?sortby=rating&order=ascending
        """
        expected = {
            'current_page': 1,
            'objects': [
                {
                    'address1': None,
                    'address2': None,
                    'address3': None,
                    'all_images': None,
                    'city': None,
                    'close_shelters': [],
                    'country': None,
                    'display_address': None,
                    'display_phone': None,
                    'hours': None,
                    'id': '4',
                    'image_url': None,
                    'latitude': None,
                    'longitude': None,
                    'name': None,
                    'pets': [],
                    'phone': None,
                    'rating': 5.0,
                    'review_count': None,
                    'state': None,
                    'website_url': None,
                    'zip_code': None},
                {
                    'address1': None,
                    'address2': None,
                    'address3': None,
                    'all_images': None,
                    'city': None,
                    'close_shelters': [],
                    'country': None,
                    'display_address': None,
                    'display_phone': None,
                    'hours': None,
                    'id': '3',
                    'image_url': None,
                    'latitude': None,
                    'longitude': None,
                    'name': None,
                    'pets': [],
                    'phone': None,
                    'rating': 3.5,
                    'review_count': None,
                    'state': None,
                    'website_url': None,
                    'zip_code': None
                },
                {
                    'address1': None,
                    'address2': None,
                    'address3': None,
                    'all_images': None,
                    'city': None,
                    'close_shelters': [],
                    'country': None,
                    'display_address': None,
                    'display_phone': None,
                    'hours': None,
                    'id': '5',
                    'image_url': None,
                    'latitude': None,
                    'longitude': None,
                    'name': None,
                    'pets': [],
                    'phone': None,
                    'rating': 3.2,
                    'review_count': None,
                    'state': None,
                    'website_url': None,
                    'zip_code': None
                }
            ],
            'total': 1}
        db.session.add(Vet(id=5, rating=3.2))
        db.session.add(Vet(id=4, rating=5.0))
        db.session.add(Vet(id=3, rating=3.5))

        db.session.commit()
        response = self.client.get(
            '/api/vet/page/1?sortby=rating&order=descending')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_VetID(self):
        """
        test for VetID()

        make sure this endpoint return Vet Class instance
        in json based on vet's id

        endpoint: /api/vet/<string:vid>
        """

        response = self.client.get('/api/vet/sM-kfaitEtWzTG1aYgSJng')
        self.assertStatus(response, 404)

    """
    Endpoint for Pet
    """

    def test_api_ShelterPage_name_sorting(self):
        """
        test for ShelterPage()

        if name sort is passed to parameter,
        sort items by name

        endpoint: /api/shelter/page/1?sortby=name&order=ascending
        """
        expected = {
            'current_page': 1,
            'objects': [
                {
                    'address1': None,
                    'address2': None,
                    'city': None,
                    'close_vets': [],
                    'email': None,
                    'id': '1',
                    'img_url': None,
                    'latitude': None,
                    'longitude': None,
                    'name': 'A',
                    'pets': [],
                    'phone': None,
                    'state': None,
                    'zip': None
                },
                {
                    'address1': None,
                    'address2': None,
                    'city': None,
                    'close_vets': [],
                    'email': None,
                    'id': '5',
                    'img_url': None,
                    'latitude': None,
                    'longitude': None,
                    'name': 'B',
                    'pets': [],
                    'phone': None,
                    'state': None,
                    'zip': None
                },
                {
                    'address1': None,
                    'address2': None,
                    'city': None,
                    'close_vets': [],
                    'email': None,
                    'id': '3',
                    'img_url': None,
                    'latitude': None,
                    'longitude': None,
                    'name': 'E',
                    'pets': [],
                    'phone': None,
                    'state': None,
                    'zip': None
                }
            ],
            'total': 1}
        db.session.add(Shelter(id=1, name='A'))
        db.session.add(Shelter(id=3, name='E'))
        db.session.add(Shelter(id=5, name='B'))
        db.session.commit()

        response = self.client.get(
            '/api/shelter/page/1?sortby=name&order=ascending')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_ShelterID_1(self):
        """
        test for ShelterID()

        make sure this endpoint return Shelter Class instance
        in json based on shelter's id

        endpoint: /api/shelter/<string:sid>
        """

        with self.app.test_request_context():
            response = self.client.get('/api/shelter/AK04')
            self.assertStatus(response, 404)

    """
    Endpoint for Dropdown
    """

    def test_api_DropdownPet_no_param_provided(self):
        """
        test for DropdownPet()

        Default state = TX, so if no param provided, then
        return list of city for TX

        endpoint: /api/pet/dropdown
        """
        expected = {'city': ['Austin', 'Dallas']}
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='TX', city='Dallas'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/dropdown')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_DropdownPet_param_state_provided(self):
        """
        test for DropdownPet()

        return: list of city for SC
        endpoint: /api/pet/dropdown?state=SC
        """
        expected = {'city': ['Aiken', 'Rock Hill']}
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='SC', city='Aiken'))
        db.session.add(Shelter(id=3, state='SC', city='Rock Hill'))
        db.session.commit()
        response = self.client.get('/api/pet/dropdown?state=SC')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    """
    Endpoint for Stats
    """

    def test_api_StatsPet(self):
        """
        test for StatsPet()

        number of dog/cat etc and total pets for each state
        also total pets for all state

        endpoint: /api/stats
        """
        db.session.add(Pet(id=1, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=2, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=3, shelter_id=1, animal='cat'))
        db.session.add(Pet(id=4, shelter_id=2, animal='dog'))
        db.session.add(Pet(id=5, shelter_id=2, animal='bird'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='SC', city='Aiken'))
        db.session.commit()

        response = self.client.get('/api/stats/pet')
        self.assertEqual(response.status_code, 200)
        # pp.pprint(response.json)

    def test_api_StatsVet(self):
        """
        how many vets in state
        how many vets whose rating btw 2 - 3, 3 - 4, 4 - 5

        endpoint: /api/stats/vet
        """

        db.session.add(Vet(id=5, rating=3.2, state='TX'))
        db.session.add(Vet(id=4, rating=5.0, state='TX'))
        db.session.add(Vet(id=3, rating=3.5, state='TX'))
        db.session.add(Vet(id=2, rating=1.2, state='SC'))

        db.session.commit()
        response = self.client.get(
            '/api/stats/vet')
        # pp.pprint(response.json)
        self.assertEqual(response.status_code, 200)

    """
    test for getPetsUtil.py
    """

    def test_num(self):
        """
        test for num()
        """
        # NEED TO WORK ON
        pass

    def test_unicode(self):
        """
        test for unicode()
        """
        # NEED TO WORK ON
        pass

    def test_stringify(self):
        """
        test for stringify()
        """
        # NEED TO WORK ON
        pass

    def test_getVetsByLocation(self):
        """
        test for getVetsByLocation()
        """
        # NEED TO WORK ON
        # USE with patch.object()
        pass

    def test_getRandomPetFromPetFinder(self):
        """
        test for getRandomPetFromPetFinder()
        """
        # NEED TO WORK ON
        # USE with patch.object()
        pass

    def test_parsePet(self):
        """
        test for parsePet()
        """
        # NEED TO WORK ON
        pass

    @mock.patch('getPetsUtil.requests.get')
    def test_getPetsFromPetFinder(self, mock_requests):
        """
        test for getPetsFromPetFinder()
        """
        jsonRespone = {
            "petfinder": {
                "pets": {
                    "pet": [{
                        "options": {},
                        "status": {},
                        "contact": {
                            "phone": {"$t": "(512) 453-8094"}, "state": {"$t": "TX"},
                            "address2": {}, "email": {"$t": "swig@sbcglobal.net"},
                            "city": {"$t": "Austin"}, "zip": {"$t": "78731"},
                            "fax": {}, "address1": {}
                        },
                        "age": {"$t": "Adult"},
                        "size": {"$t": "M"},
                        "media": {
                            "photos": {
                                "photo": [
                                    {"@size": "x",
                                     "$t": "http://photos.petfinder.com/photos/pets/38775931/1/?bust=1500156669&width=60&-pnt.jpg",
                                     "@id": "1"
                                     }]}},
                        "id": {"$t": "38775931"},
                        "shelterPetId": {},
                        "breeds": {"breed": {"$t": "Shetland Sheepdog Sheltie"}
                                   },
                        "name": {"$t": "Robin"},
                        "sex": {"$t": "M"},
                        "description": {
                            "$t": "Robin also came from a North Texas Shelter and was shaved down before coming into rescue.  His coat is growing in and it is looking great.  He is between 6 and 8 years old and is a good weight.  He is neutered, up to date on all vaccinations, heartworm negative and on heartworm preventive.  He seems to be a mellow fellow and he walks well on a leash.  He is working on housetraining."
                        },
                        "mix": {"$t": "no"},
                        "shelterId": {"$t": "TX1196"},
                        "lastUpdate": {
                            "$t": "2017-07-15T22:10:20Z"
                        }, "animal": {"$t": "Dog"}
                    }]
                }}}

        """
        test for getPetsFromPetFinder()
        """
        # NEED TO WORK ON
        # USE with patch.object()

        mock_requests.return_value = mock_response = Mock()
        mock_response.status_code = 200
        mock_response.text = json.dumps(jsonRespone)

        pList = getPetsFromPetFinder("dog", 10, "78731")

        for pet in pList:
            self.assertEqual(pet.name.decode('utf-8'), "Robin")
            self.assertEqual(pet.shelter_id, "TX1196")
            self.assertEqual(pet.sex.decode('utf-8'), "M")

    def test_getPetsNonSpecific(self):
        """
        test for getPetsNonSpecific()
        """
        # NEED TO WORK ON
        # USE with patch.object()
        pass

    def test_getShelterByIdFromPetFinder(self):
        """
        test for getShelterByIdFromPetFinder()
        """
        # NEED TO WORK ON
        # USE with patch.object()
        pass

    def test_getPetByIdFromPetFinder(self):
        """
        test for getPetByIdFromPetFinder()
        """
        # NEED TO WORK ON
        # USE with patch.object()
        pass

    """
    tests for Elasticsearch
    """

    def test_SearchPet(self):
        pass
        # with testing.elasticsearch.Elasticsearch() as elasticsearch:
        #     # connect to Elasticsearch (using elasticsearch-py)
        #     es = Elasticsearch(**elasticsearch.dsn())
        #     create_pet_index(es)
        #     pet = Pet(id=1, shelter_id=1, animal='dog')
        #     pet_data_migration((pet, 'Austin', 'TX', 'a', 'a'), es)
        #     print(pet_search('', 1), es)

        # response = self.client.get('/api/shelter/search?q=Texas shelter&size=1&page=2')
        # pp.pprint(response.json)
        # self.assertEqual(response.json, expected)
        # self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
