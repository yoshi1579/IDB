flask
autopep8
coverage
mypy
numpy
pylint
quotes
mysql-connector-python
#http://dev.mysql.com/get/Downloads/Connector-Python/mysql-connector-python-2.1.4.zip
flask_sqlalchemy
pymysql
Flask-Restless
