from elasticsearch import Elasticsearch
#client = Elasticsearch('localhost:9200')
client = Elasticsearch('https://search-connectpetstome-duzal535fd4ilkkjotehb54qxe.us-east-1.es.amazonaws.com')
import pprint
pp = pprint.PrettyPrinter(indent=1)

client.indices.delete(index='connectpetstome_shelter', ignore=[400, 404])

def create_shelter_index():
    mappings = {
        "settings": {
            "index": {
                "analysis": {
                    "analyzer": {
                        "customHTMLSnowball": {
                        "type": "custom", 
                            "char_filter": [
                            "html_strip"
                            ], 
                            "tokenizer": "standard",
                            "filter": [
                            "lowercase",
                            "asciifolding",
                            "stop", 
                            "snowball"
                            ]  
                        }
                    }
                }
            }
        },
        'mappings': {
            'shelter': {
                "_all": {
                    "enabled": True,
                    "store": True,
                    'analyzer': 'customHTMLSnowball'
                },
                'properties': {
                    'type': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'name': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'address1': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'address2': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'city': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'state': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'zip': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'phone': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'img_url': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    }
                }
            }
        }
    }
    client.indices.create(index='connectpetstome_shelter', body=mappings)
