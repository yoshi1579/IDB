from elasticsearch import Elasticsearch
#client = Elasticsearch('localhost:9200')
client = Elasticsearch('https://search-connectpetstome-duzal535fd4ilkkjotehb54qxe.us-east-1.es.amazonaws.com')
import pprint
pp = pprint.PrettyPrinter(indent=1)

# client.indices.delete(index='connectpetstome_pet', ignore=[400, 404])


"""
TO JEFFREY

Now, three pillar has attibute named 'type' in the schema below.

'type' -> either 'pet', 'vet veterinarian veterinary', 'animal shelter'
This above is usefull because when user type 'pet dog' then, this type will catch 'pet'
'vet veterinarian veterinary' is kinda long, but this way we don't need to deal with synonym dictionary (ES offers but ... we don't have a time)

About 'rating' in vet schema, instead of number, 

insert 
"best" if 4.5 <= x <= 5
"good" if 3.5 <= x < 4.5
"normal" otherwise

so user can search "Best vet in texas"

Now if you run, client.indices.create(index='connectpetstome_pet', body=mappings) then 3 tables(index) will be created
if you have any question, let me know

"""

def create_pet_index(es=None):
    if es == None:
        es = client
    es.indices.delete(index='connectpetstome_pet', ignore=[400, 404])
    mappings = {
        "settings": {
            "index": {
                "analysis": {
                    "analyzer": {
                        "customHTMLSnowball": {
                        "type": "custom", 
                            "char_filter": [
                            "html_strip"
                            ], 
                            "tokenizer": "standard",
                            "filter": [
                            "lowercase",
                            "asciifolding",
                            "stop", 
                            "snowball"
                            ]  
                        }
                    }
                }
            }
        },
        'mappings': {
            'pet': {
                "_all": {
                    "enabled": True,
                    "store": True,
                    'analyzer': 'customHTMLSnowball'
                },
                'properties': {
                    'type': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'name': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'breed': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'sex': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'animal': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'age': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'size': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'description': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'city': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'state': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'temperament': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'detail': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'image_url': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    }
                }
            }
        }
    }
    es.indices.create(index='connectpetstome_pet', body=mappings)
