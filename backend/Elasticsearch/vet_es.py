from elasticsearch import Elasticsearch
#client = Elasticsearch('localhost:9200')
client = Elasticsearch('https://search-connectpetstome-duzal535fd4ilkkjotehb54qxe.us-east-1.es.amazonaws.com')
import pprint
pp = pprint.PrettyPrinter(indent=1)


def create_vet_index():
    client.indices.delete(index='connectpetstome_vet', ignore=[400, 404])
    mappings = {
        "settings": {
            "index": {
                "analysis": {
                    "analyzer": {
                        "customHTMLSnowball": {
                        "type": "custom", 
                            "char_filter": [
                            "html_strip"
                            ], 
                            "tokenizer": "standard",
                            "filter": [
                            "lowercase",
                            "asciifolding",
                            "stop", 
                            "snowball"
                            ]  
                        }
                    }
                }
            }
        },
        'mappings': {
            'vet': {
                "_all": {
                    "enabled": True,
                    "store": True,
                    'analyzer': 'customHTMLSnowball'
                },
                'properties': {
                    'id': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    },
                    'type': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'name': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'address1': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'address2': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'address3': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'city': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'state': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'country': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'zip_code': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'display_phone': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'rating': {
                        'type': 'string',
                        'analyzer': 'customHTMLSnowball',
                        "store": True
                    },
                    'image_url': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    }
                }
            }
        }
    }
    client.indices.create(index='connectpetstome_vet', body=mappings)

