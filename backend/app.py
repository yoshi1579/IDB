#!/usr/bin/env python3

"""
Import Statements
"""

import json
import os
import pickle
import random
import statistics
from pathlib import Path

import mysql.connector
from flask import Flask, Response, jsonify, render_template, request
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_restful import Api, Resource, reqparse
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from mysql.connector import errorcode
from sqlalchemy import func

import database
from database import *
from Elasticsearch.query_es import *

"""
App Configurations
"""

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

# Set database info based on dev or production
prod = os.environ.get("PRODUCTION", None)
if not prod:
    db_name = os.environ.get("DEV_DB", None)
    username = os.environ.get("DEV_USERNAME", None)
    password = os.environ.get("DEV_PASSWORD", None)
    hostname = os.environ.get("DEV_HOSTNAME", None)
else:
    db_name = os.environ.get("RDS_DB_NAME", None)
    username = os.environ.get("RDS_USERNAME", None)
    password = os.environ.get("RDS_PASSWORD", None)
    hostname = os.environ.get("RDS_HOSTNAME", None)
options = "charset=utf8mb4"

# If you comment this in, you will connect production database
db_name = os.environ.get("RDS_DB_NAME", None)
username = os.environ.get("RDS_USERNAME", None)
password = os.environ.get("RDS_PASSWORD", None)
hostname = os.environ.get("RDS_HOSTNAME", None)

# SQLAlchemy configurations
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# if db_name == None, don't connect to database. This is useful for testing
if db_name != None:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + username + \
        ':' + password + '@' + hostname + '/' + db_name + '?' + options
else:
    print("** Not connected to production database for testing **")

# Initialize database and marshmallow
database.db.init_app(app)
database.ma.init_app(app)
# Initialize API
api = Api(app)

"""
Flask Routes
"""


@app.route('/create_table', methods=['GET'])
def create():

    # calling function from database.py
    database.create_table(app)
    return render_template('index.html', message="created tables!!"), 200


@app.route('/insert_data', methods=['GET'])
def insert():

    # calling function from database.py
    database.insert_data(app)
    return render_template('index.html', message="Data inserted!!")


@app.route('/es_create_index', methods=['GET'])
def es_create_index():

    # calling function from database.py
    database.es_create_index(app)
    return render_template('index.html', message="ES Index created!!")


@app.route('/es_insert_data', methods=['GET'])
def es_insert_data():

    # calling function from database.py
    database.es_data_migration(app)
    return render_template('index.html', message="ES data inserted!!")


@app.route('/d3bubble')
def d3bubble(message=None):
    return render_template('bubble.html', message=message), 200

# """""""""""
# Guardians of the Wild Visualizations: Data getter functions
# """""""""""


@app.route('/taxonomy_data')
def get_taxonomy_data():
    with open('templates/GuardiansOfTheWild_Visualizations/tax.json', 'r') as f:
        result = (json.load(f))
    return jsonify(result)


@app.route('/threats_data')
def get_threats_data():
    with open('templates/GuardiansOfTheWild_Visualizations/threats_formatted.json', 'r') as f:
        result = (json.load(f))
    return jsonify(result)


@app.route('/location_data')
def get_location_data():
    with open('templates/GuardiansOfTheWild_Visualizations/location.json', 'r') as f:
        result = (json.load(f))
    return jsonify(result)

# """""""""""
# Guardians of the Wild Visualizations: Page getter functions
# """""""""""


@app.route('/taxonomy')
def get_taxonomy_page():
    return render_template('taxonomy.html')


@app.route('/locations')
def get_locations_page():
    return render_template('locations.html')


@app.route('/threats')
def get_threats_page():
    return render_template('threats.html')


@app.route("/")
def render(message=None):
    return render_template('index.html', message={'TX': 1}), 200


"""
Globales
"""
# Pagination
PER_PAGE = 9

"""
API: Helper functions
"""


def num_pages(n):
    """
    Calculate the number of pages in 'n' records
    based on the constatnt 'PER_PAGE'
    """
    return (n // PER_PAGE) if (n % PER_PAGE) == 0 else (n // PER_PAGE + 1)


def filter_by(item, id, many):
    """
    SQLAlchemy helper method
    Arguments:
        item {string} -- Model flag
        id {string} -- string query argument
        many {boolean} -- Model schema argument
    Returns:
        JSON -- A representation of the sqlalchemy query result.
    """

    if item == 'pid':
        item_str = 'pet'
        query = Pet.query.filter_by(id=id).first_or_404()
        query_schema = PetSchema(many=many)
    elif item == 'vid':
        item_str = 'vet'
        query = Vet.query.filter_by(id=id).first_or_404()
        query_schema = VetSchema(many=many)
    elif item == 'sid':
        item_str = 'shelter'
        query = Shelter.query.filter_by(id=id).first_or_404()
        query_schema = ShelterSchema(many=many)
    elif item == 'bid':
        item_str = 'breed'
        query = Breed.query.filter_by(id=id).first_or_404()
        query_schema = BreedSchema(many=many)
    else:
        return msg_no_content()
    return jsonify({item_str: query_schema.dump(query).data})


def msg_no_content():
    """
    Custom JSON 204 message
    """
    message = "{ERROR MSG : NO CONTENT FOUND, 204}"
    return jsonify({'ERROR MSG': 'NO CONTENT FOUND'}), 204


"""
 API: Pet
"""


class PetID(Resource):
    """
    Returns a json representation of a pet in our Pet model
    Arguments:
        Resource {Integer} -- pid: Pet ID
    Returns:
        JSON -- pet attributes
    """

    def get(self, pid=None):
        if not pid:
            return msg_no_content()
        return filter_by('pid', pid, False)


def parse_params(params):
    """
    input: dict -> query parameter

    parse dict of query parametes and create three dicts
    1: condition for pet
    2: condition for shelter
    3: condition for vet
    return 4 dicts
    """
    p_condition = set(['animal', 'age', 'sex', 'size'])
    s_condition = set(['city', 'state'])
    v_condition = set(['rating'])
    # Default State = TX
    p_dict = {}
    s_dict = {}
    v_dict = {}
    sort_dict = {}

    temp: str = None
    for key, value in params.items():
        if key in p_condition and value:
            p_dict[key] = value
        elif key in s_condition and value:
            s_dict[key] = value
        elif key in v_condition and value:
            v_dict[key] = value
        elif key == 'sortby' and value:
            if temp:
                sort_dict[value.lower()] = temp
            else:
                temp = value
        elif key == 'order' and value:
            if temp:
                sort_dict[temp.lower()] = value
            else:
                temp = value

    # Set the default rating (no rating filter selected) to 0
    if 'rating' not in v_dict:
        v_dict['rating'] = 0

    return p_dict, s_dict, v_dict, sort_dict


def add_query_params(parser, param_list_str=None, param_list_int=None):
    """[summary]

    Arguments:
        parser {[type]} -- [description]

    Keyword Arguments:
        param_list_str {[type]} -- [description] (default: {None})
        param_list_int {[type]} -- [description] (default: {None})
    """

    if param_list_str is None:
        param_list_str = []
    if param_list_int is None:
        param_list_int = []

    for param in param_list_str:
        parser.add_argument(param, type=str)
    for param in param_list_int:
        parser.add_argument(param, type=int)


def sorting(query, data_class, sort_key, value='ascending'):
    """
    based on input parameter in url, sort query results
    """
    # NOTE: 'ascending' is a long name, but we can change it later
    # 		  in Phase 4. Someone has to change it in front-end though.
    if sort_key == 'size':
        if value == 'ascending':
            return query.order_by(
                db.case(
                    ((data_class.size == 'S', 1),
                     (data_class.size == 'M', 2),
                        (data_class.size == 'L', 3),
                        (data_class.size == 'XL', 4),
                     ), else_=5).asc())
        else:
            return query.order_by(
                db.case(
                    ((data_class.size == 'S', 1),
                     (data_class.size == 'M', 2),
                     (data_class.size == 'L', 3),
                     (data_class.size == 'XL', 4),
                     ), else_=5).desc()
            )
    elif sort_key == 'name':
        if value == 'ascending':
            return query.order_by(data_class.name.asc())
        else:
            return query.order_by(data_class.name.desc())

    elif sort_key == 'rating':
        if value == 'ascending':
            return query.order_by(data_class.rating.asc())
        else:
            return query.order_by(data_class.rating.desc())
    else:
        return query


class PetPage(Resource):
    """[summary]

    Arguments:
        Resource {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    def get(self, pg=None):
        if not pg:
            return msg_no_content()

        # get Query parameters
        parser = reqparse.RequestParser()
        add_query_params(
            parser,
            param_list_str=['animal', 'age', 'sex',
                            'size', 'city', 'state', 'sortby', 'order']
        )
        pet_cond, shel_cond, _, sort_conds = parse_params(parser.parse_args())

        # filtering
        q = database.db.session.query(Pet).filter_by(**pet_cond).\
            join(Shelter, Pet.shelter_id == Shelter.id).filter_by(**shel_cond)

        # sorting
        for key, value in sort_conds.items():
            q = sorting(q, Pet, key, value)

        pages = q.paginate(page=pg, per_page=PER_PAGE, error_out=True)
        pages_schema = PetSchema(many=True)
        pets = pages_schema.dump(pages.items)
        return jsonify(
            {
                'objects': pets.data,
                'total_pages': num_pages(pages.total),
                'page': pages.page
            }
        )

# """"""""""""
# API: Vet
# """"""""""""


class VetID(Resource):
    """[summary]

    Arguments:
        Resource {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    def get(self, vid=None):
        if not vid:
            return msg_no_content()
        return filter_by('vid', vid, False)


class VetPage(Resource):
    """[summary]

    Arguments:
        Resource {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    def get(self, pg=None):
        if not pg:
            return msg_no_content()

        # get Query parameters
        parser = reqparse.RequestParser()
        add_query_params(
            parser,
            param_list_str=['city', 'state', 'sortby', 'order'],
            param_list_int=['rating']
        )
        _, shel_cond, vet_cond, sort_conds = parse_params(parser.parse_args())

        # filtering
        q = database.db.session.query(Vet).filter_by(**shel_cond).\
            filter(Vet.rating >= vet_cond['rating'])
        # sorting
        for key, value in sort_conds.items():
            q = sorting(q, Vet, key, value)

        pages = q.paginate(page=pg, per_page=PER_PAGE, error_out=True)

        pages_schema = VetSchema(many=True)
        vets = pages_schema.dump(pages.items)
        return jsonify(
            {'objects': vets.data,
             'total': num_pages(pages.total),
             'current_page': pages.page}
        )

# """"""""""""
# API: Shelter
# """"""""""""


class ShelterID(Resource):
    """[summary]

    Arguments:
        Resource {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    def get(self, sid=None):
        if not sid:
            return msg_no_content()
        return filter_by('sid', sid, False)


class ShelterPage(Resource):
    """[summary]

    Arguments:
        Resource {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    def get(self, pg=None):
        if not pg:
            return msg_no_content()

        # get Query parameters
        parser = reqparse.RequestParser()
        add_query_params(
            parser,
            param_list_str=['city', 'state', 'sortby', 'order']
        )
        _, shel_cond, _, sort_conds = parse_params(parser.parse_args())

        # filtering
        q = database.db.session.query(Shelter).filter_by(**shel_cond)

        # sorting
        for key, value in sort_conds.items():
            q = sorting(q, Shelter, key, value)

        pages = q.paginate(page=pg, per_page=PER_PAGE, error_out=True)
        pages_schema = ShelterSchema(many=True)
        shelters = pages_schema.dump(pages.items)
        return jsonify(
            {'objects': shelters.data,
             'total': num_pages(pages.total),
             'current_page': pages.page}
        )

# """"""""""""
# API: Breed
# """"""""""""


class BreedID(Resource):
    """
    Return breed temperament and detail
    based on breed name
    """

    def get(self, bid=None):
        if not bid:
            return msg_no_content()
        return filter_by('bid', bid, False)

# """"""""""""
# API: Dropdown
# """"""""""""


class DropdownPet(Resource):
    """
    ex: state=TX -> return list of city for TX
        anima=Dog -> return list of breed for Dog
    """

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('state', type=str)

        shel_cond = parse_params(parser.parse_args())[1]
        query = Shelter.query.with_entities(Shelter.city).\
            filter_by(**shel_cond).distinct()
        states = [state[0] for state in query]

        return jsonify({'city': states})

# """"""""""""
# API: Elasticsearch
# """"""""""""


def set_query_params():
    """
    set query parameters
    ex: ?q=dog pet?page=1?size=3
    """
    parser = reqparse.RequestParser()
    parser.add_argument('q', type=str)
    parser.add_argument('page', type=int)
    parser.add_argument('size', type=int)
    return parser


class SearchPet(Resource):
    """
    Use Elasticsearch to search pets based on a search query from user

    endpoint: /api/pet/search?q=happy pet&page=1
    """

    def get(self):
        parser = set_query_params()
        result = pet_search(**parser.parse_args())
        return jsonify(result['hits'])


class SearchVet(Resource):
    """
    Use Elasticsearch to search vets based on a search query from user

    endpoint: /api/vet/search?q=happy vet&page=1
    """

    def get(self):
        parser = set_query_params()
        result = vet_search(**parser.parse_args())
        return jsonify(result['hits'])


class SearchShelter(Resource):
    """
    Use Elasticsearch to search shelters based on a search query from user

    endpoint: /api/shelter/search?q=happy shelter&page=1
    """

    def get(self):
        parser = set_query_params()
        result = shelter_search(**parser.parse_args())
        return jsonify(result['hits'])

# """"""""""""
# API: Stats
# """"""""""""


class StatsPet(Resource):
    """
    show how many dogs, cats, birds and etc
    for each state.
    Also show total pets for all states and each state

    endpoint: /api/stats
    """

    def get(self):

        my_file = Path("cache/pet_stats.pickle")
        if my_file.is_file():
            # cache file exists
            with open('cache/pet_stats.pickle', 'rb') as handle:
                data = pickle.load(handle)
        else:
            # cache file not exist
            # get total pets for each state
            query = db.session.query(Shelter.state, func.count(Pet.id)).\
                join(Pet, Pet.shelter_id == Shelter.id).group_by(Shelter.state)

            states_stats = {}
            totals = []
            for state, total_pet in query:
                # get total dog, cat etc for each state
                q = db.session.query(Pet.animal, func.count(Pet.id)).\
                    join(Shelter, Pet.shelter_id == Shelter.id).filter_by(
                        state=state).group_by(Pet.animal)
                animal_stats = {key: value for key, value in q}
                animal_stats['total'] = total_pet
                states_stats[state] = animal_stats
                totals.append(total_pet)

            total_all_state = Pet.query.count()
            data = {'stats': states_stats,
                    'total': total_all_state,
                    'median': statistics.median(totals)
                    }

            # create a cache file
            with open('cache/pet_stats.pickle', 'wb') as handle:
                pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

        return jsonify(data)


class StatsVet(Resource):
    """
    how many vets in state
    how many vets whose rating btw 2 - 3, 3 - 4, 4 - 5

    endpoint: /api/stats/vet
    """

    def get(self):
        my_file = Path("cache/vet_stats.pickle")
        if my_file.is_file():
            # cache file exists
            with open('cache/vet_stats.pickle', 'rb') as handle:
                data = pickle.load(handle)
        else:
            # cache file not exist
            # get total pets for each state
            query = db.session.query(Vet.state, func.count(Vet.id)).\
                group_by(Vet.state)

            states_stats = {}
            totals = []
            for state, total_vet in query:
                # get total vets for each rating interval
                vet_stats = {}
                for i in range(0, 5):
                    q = Vet.query.filter_by(state=state).filter(Vet.rating <= i + 1).\
                        filter(Vet.rating > i).count()
                    vet_stats[str(i) + '-' + str(i + 1)] = q
                vet_stats['total'] = total_vet
                states_stats[state] = vet_stats
                totals.append(total_vet)

            total_all_state = Vet.query.count()
            data = {'stats': states_stats,
                    'total': total_all_state,
                    'median': statistics.median(totals)
                    }
            # create a cache file
            with open('cache/vet_stats.pickle', 'wb') as handle:
                pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

        return jsonify(data)


class StatsShelter(Resource):
    """
    show how many dogs, cats, birds and etc
    for each state.
    Also show total pets for all states and each state

    endpoint: /api/stats/shelter
    """

    def get(self):

        my_file = Path("cache/shelter_stats.pickle")
        if my_file.is_file():
            # cache file exists
            with open('cache/shelter_stats.pickle', 'rb') as handle:
                data = pickle.load(handle)
        else:
            # cache file not exist
            # get total pets for each state
            query = db.session.query(Shelter.state, func.count(Shelter.id)).\
                group_by(Shelter.state)

            states_stats = {}
            totals = []
            for state, total_shelter in query:
                # get total dog, cat etc for each state
                q = db.session.query(Pet.animal, func.count(Pet.id)).\
                    join(Shelter, Pet.shelter_id == Shelter.id).filter_by(
                        state=state).group_by(Pet.animal)
                animal_stats = {key: value for key, value in q}
                animal_stats['total'] = total_shelter
                states_stats[state] = animal_stats
                totals.append(total_shelter)

            total_all_state = Shelter.query.count()
            data = {'stats': states_stats,
                    'total': total_all_state,
                    'median': statistics.median(totals)
                    }

            # create a cache file
            with open('cache/shelter_stats.pickle', 'wb') as handle:
                pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

        return jsonify(data)


class D3(Resource):
    """
    Serve D3 Data Visualizations
    Arguments:
        Resource {Query String} -- q: query; model:model
    Returns:
        csv file -- structured for the d3 template use
    """

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('q', type=str)
        parser.add_argument('model', type=str)
        args = parser.parse_args()

        # PET MODEL - animal type
        if args['model'] == 'pet':
            # animal type
            if args['q'] == 'animal':
                qry = db.engine.execute(
                    'select animal, count(*)\
                    from pet\
                    group by animal\
                    order by count(animal)')
            # pets per city
            elif args['q'] == 'city':
                qry = db.engine.execute(
                    'select city as id, count(*) as value\
                    from pet p join shelter s\
                        on p.shelter_id = s.id\
                    group by s.city')
            # pets per state
            elif args['q'] == 'state':
                qry = db.engine.execute(
                    'select state as id, count(*) as value\
                    from pet p join shelter s\
                        on p.shelter_id = s.id\
                        group by id\
                        order by id')
        # SHELTER MODEL
        if args['model'] == 'shelter':
            # shelter per city
            if args['q'] == 'city':
                qry = db.engine.execute(
                    'select city as id, count(*) as value\
                    from shelter\
                    group by city')
            # shelters per state
            elif args['q'] == 'state':
                qry = db.engine.execute(
                    'select state as id, count(*) as value\
                    from shelter\
                    group by state')
        # VET MODEL
        if args['model'] == 'vet':
            # vets per city
            if args['q'] == 'city':
                qry = db.engine.execute(
                    'select city as id, count(*) as value\
                    from vet\
                    group by city')
            # vets per state
            elif args['q'] == 'state':
                qry = db.engine.execute(
                    'select state as id, count(*) as value\
                    from vet\
                    group by state')
            # vets by ranking
            elif args['q'] == 'rank':
                qry = db.engine.execute(
                    'select state as id, count(*) as value\
                    from vet\
                    where rating = 5.0\
                    group by state')

        # JSON build
        # r = list()
        # for i in qry:
        #     r_in = dict()
        #     r_in['id'] =i[0]
        #     r_in['value'] = str(i[1])
        #     r.append(r_in)

        # CVS build
        r = 'id,value\n'
        for i in qry:
            r += i[0] + ','
            r += str(i[1]) + '\n'

        # JSON Response
        # return jsonify(r)

        # CSV Response
        return Response(
            r,
            mimetype="text/csv",
            headers={"Content-disposition": "attachment; filename=response.csv"}
        )


# Pet API
api.add_resource(PetID, '/api/pet/<int:pid>')

api.add_resource(PetPage, '/api/pet/page/<int:pg>')
# Elasticsearch
api.add_resource(SearchPet, '/api/pet/search')
# Dropdown
api.add_resource(DropdownPet, '/api/pet/dropdown')

# Vet API
api.add_resource(VetID, '/api/vet/<string:vid>')
api.add_resource(VetPage, '/api/vet/page/<int:pg>')
# Elasticsearch
api.add_resource(SearchVet, '/api/vet/search')

# Shelter API
api.add_resource(ShelterID, '/api/shelter/<string:sid>')
api.add_resource(ShelterPage, '/api/shelter/page/<int:pg>')
# Elasticsearch
api.add_resource(SearchShelter, '/api/shelter/search')

# Breed API
api.add_resource(BreedID, '/api/breed/<string:bid>')

# Stats API
api.add_resource(StatsPet, '/api/stats/pet')
api.add_resource(StatsVet, '/api/stats/vet')
api.add_resource(StatsShelter, '/api/stats/shelter')

# D3.js
api.add_resource(D3, '/api/d3')

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
