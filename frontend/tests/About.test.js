import expect from 'expect';
import About from './../src/Pages/About';
import React from 'react';
import { shallow } from 'enzyme';

const wrapper = shallow(<About />);



describe("Tests About.js", () => {
  test("Renders without exploding", () => {
    expect(wrapper.length).toBe(1);
  });

  test("Make sure that Developer component loaded", () => {
      expect(wrapper.find('#dev-cards').length).toBe(1);
  });
});
