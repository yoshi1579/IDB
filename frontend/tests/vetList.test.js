import expect from 'expect';
import vetList from './../src/Pages/ListingPage/VetList/vetList';
import React from 'react';
import { shallow, mount, render } from 'enzyme';

describe("Tests vetList.js", () => {

  test("Renders without exploding", () => {

    const wrapper = shallow(<vetList />);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

  const minProps = {
    vets: [
        {
            id: "0",
            name: "Emancipet",
            image: "https://s3-media3.fl.yelpcdn.com/bphoto/wLB-AA3Dt15HeHs1OOxGGQ/ls.jpg",
            location: "Austin",
            rating: "4",
            contact: "(512) 587-7729"
        },
        {
            id: "1",
            name: "Zippivet",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/S2LYJ96acZeO62hfcfzHjQ/ls.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 904-0218"
        },
        {
            id: "2",
            name: "PAZ Veterinary",
            image: "https://s3-media4.fl.yelpcdn.com/bphoto/MvDi_2SFJk8WYtCdRCnIEA/o.jpg",
            location: "Austin",
            rating: "5",
            contact: "(512) 236-8000"
        },
        {
            id: "3",
            name: "Austin Urban Vet Center ",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/6cnIoBmQyIFAtwCghNODDg/ls.jpg",
            location: "Austin",
            rating: "4",
            contact: "(512) 476-2882"
        }
    ],
  }

  test("Takes vets as props", () => {
    const wrapper = shallow(<vetList {...minProps}/>);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

});
