import expect from 'expect';
import Carousel from './../src/Pages/Home/Carousel';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests Carousel.js", () => {
  test("Renders without exploding", () => {
    const wrapper = shallow(<Carousel />);
    expect(wrapper.length).toBe(1);
  });

  test("Has at least three carousel items", () => {
    const wrapper = shallow(<Carousel />);
    expect(wrapper.find('.carousel-item').length).toBeGreaterThanOrEqual(3);
  });
});
