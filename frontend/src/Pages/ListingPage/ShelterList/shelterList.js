import React, { Component } from 'react';
import ShelterItems from './../../../Components/Model/Shelters/ShelterItems';
import PageBar from './../../../Components/PageBar';
import PageHeader, {HeaderItem} from './../../../Components/PageHeader';
import { fetchData } from './../../../Actions/dataModelAction'
import {connect} from 'react-redux';

export class shelterList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          "current_page": 1,
        };
    }
      
      // get total number of pages
      // fetchData also populates this.props.shelters with current page of shelters
    componentWillMount() {
        this.props.fetchData('shelter', this.props.match.params.pageNum, this.props.location.search);
        this.setState({current_page: this.props.match.params.pageNum});
    }
    // check if page number has changed, or query string has changed
    componentDidUpdate(prevProps){
        if (prevProps.match.params.pageNum !== this.props.match.params.pageNum || prevProps.location.search !== this.props.location.search){
            this.setState({current_page: this.props.match.params.pageNum});
            this.props.fetchData('shelter', this.props.match.params.pageNum, this.props.location.search); 
        }
    }

    render() {    
        let shelterItems = this.props.shelters.map(shelter => {
        return(
        <ShelterItems shelterData = {shelter} key={shelter.id}/>
      );
    });
        let pageNum = this.props.match.params.pageNum;
        return (
            <div className="album py-5 bg-light listingPage">
                <div className="container fillPage">                    
                    <PageHeader heading="Shelters" domain="/FindShelters/" query={this.props.location.search}>
                        <HeaderItem domain="/FindShelters/" query={this.props.location.search} type="state"/>
                        <HeaderItem domain="/FindShelters/" query={this.props.location.search} type="city"/>
                    </PageHeader>
                    <br />
                    <br />
                    <div className="row">
                        {shelterItems}
                    </div>
                </div>
                <br />
                <PageBar domain="/FindShelters/" pageNum={pageNum} totalPages = {this.props.total_page} query={this.props.location.search}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
  shelters: state.shelters.shelter_items,
  total_page: state.shelters.shelter_last_page,
  current_page: state.shelters.shelter_current_page
});

export default connect(mapStateToProps, { fetchData })(shelterList)