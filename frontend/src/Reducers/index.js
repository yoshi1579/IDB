import {combineReducers} from 'redux';
import dataModelReducer from './dataModelReducer';
import petInfoReducer from './petInfoReducer';
import vetInfoReducer from './vetInfoReducer';
import shelterInfoReducer from './shelterInfoReducer';

export default combineReducers({
    pets: dataModelReducer,
    total_page: dataModelReducer,
    current_page: dataModelReducer,
    vets: dataModelReducer,
    shelters: dataModelReducer,
    petInfo: petInfoReducer,
    vetInfo: vetInfoReducer,
    shelterInfo: shelterInfoReducer,
});
