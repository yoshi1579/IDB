import { FETCH_SHELTER_ENTRY} from './../Actions/type';

const initialState = {
    items: [],
    item: {}
};

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_SHELTER_ENTRY:
            return{
                ...state,
                items: action.data.shelter
            };
        default:
            return state;
    }
}
