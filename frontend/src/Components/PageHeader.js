import React, {Component} from 'react';
import {
  mapToDisplay,
  mapSizeToDisplay
} from './../Assets/StaticData'
import {Link, withRouter} from 'react-router-dom';
import queryString from 'query-string';
import {DropItem} from './DropItem';

/*
  header bar for displaying filters/sort types/search
*/
class PageHeader extends Component{
  constructor(props) {
    super(props);

    this.style = {
      backgroundColor: "#efebe9",
    }
    this.searchUpdate = this.searchUpdate.bind(this)

    this.state = {
      q: ""
    }
    this.searchDomain = "";
    switch(this.props.domain) {
      case '/FindPets/': this.searchDomain = '/SearchPets/1?'; break;
      case '/FindVets/': this.searchDomain = '/SearchVets/1?'; break;
      case '/FindShelters/': this.searchDomain = '/SearchShelters/1?'; break;
      default: this.searchDomain='/Search/1?';
    }
  }

  searchUpdate(event){
    this.setState({q: event.target.value});
  }


  submitSearch(event) {
    if (event.key === 'Enter'){
      this.props.history.push(this.searchDomain.concat(queryString.stringify(this.state)));
    }
  }

  render(){
    this.queryObject = queryString.parse(this.props.query);
    let resetSort = queryString.parse(this.props.query);
    delete(resetSort['sortby']);
    delete(resetSort['order']);

    if (this.queryObject.order !== undefined) {
      var oppositeOrder = queryString.parse(this.props.query);
      if (oppositeOrder.order === 'ascending')
        oppositeOrder.order = 'descending';
      else
        oppositeOrder.order = 'ascending';
    }

    return (
      <div>
        <nav className="navbar navbar-light navbar-expand-lg" style={this.style}>
          <span className="navbar-brand">
            {this.props.heading}
          </span>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" id="listingToggle"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              {this.props.children}
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="" id="sortDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {this.queryObject.sortby === undefined ? "Sort By" : "Sorted by: ".concat(this.queryObject.sortby)}
                </a>
                <div className="dropdown-menu scrollable-menu" aria-labelledby="navbarDropdown">
                  <Link className="dropdown-item" to={this.props.domain.concat('1?', queryString.stringify(resetSort))}>Reset Sort</Link>
                  <DropItem domain={this.props.domain} type="sortby" queries={this.props.query}/>
                </div>
              </li>
              {this.queryObject.sortby === undefined ? null : 
              <li className="nav-item">
                <Link className="nav-link" to={this.props.domain.concat('1?', queryString.stringify(oppositeOrder))} role="button" aria-haspopup="true" aria-expanded="false">
                  {this.queryObject.order === 'ascending' ? "↑" : "↓"}
                </Link>
              </li>}
            </ul>
            <form className="form-inline my-2 my-lg-0">
              <input className="form-control" type="search" placeholder={"Search ".concat(this.props.heading)}value={this.state.q} onChange={this.searchUpdate.bind(this)} onKeyPress={this.submitSearch.bind(this)}id="header-search" />
              <Link to={this.searchDomain.concat(queryString.stringify(this.state))}>
                <button className="btn" type="submit">Search</button>
              </Link>
            </form>
          </div>
        </nav>
      </div>
    )
  }
}

/*
Component for displaying what type of options are available/selected
*/
export class HeaderItem extends Component {
  render(){
    this.queryObject = queryString.parse(this.props.query);
    let reset = queryString.parse(this.props.query);
    if (this.props.type === 'state')
      delete(reset['city']);
    delete(reset[this.props.type]);
    let upperCaseType = this.props.type.charAt(0).toUpperCase() + this.props.type.slice(1);
    if (this.props.type === 'city' && this.queryObject['state'] === undefined)
      return null;      
    let displayName = "";
      switch(this.props.type) {
        case 'state': displayName = mapToDisplay[this.queryObject[this.props.type]]; break;
        case 'rating': displayName = mapToDisplay[this.queryObject[this.props.type]]; break;
        case 'sex': displayName = mapToDisplay[this.queryObject[this.props.type]]; break;
        case 'size': displayName = mapSizeToDisplay[this.queryObject[this.props.type]]; break;
        default: displayName = this.queryObject[this.props.type];
      }
    return (
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {this.queryObject[this.props.type] === undefined ? "Filter by ".concat(upperCaseType) : upperCaseType.concat(': ', displayName)}
        </a>
        <div className="dropdown-menu scrollable-menu" aria-labelledby="navbarDropdown">
          <Link className="dropdown-item" to={this.props.domain.concat('1?', queryString.stringify(reset))}>Reset Filter</Link>
          {
            this.queryObject['state'] === undefined ? 
            <DropItem domain={this.props.domain} type={this.props.type} queries={this.props.query}/> : 
            <DropItem domain={this.props.domain} type={this.props.type} queries={this.props.query} selectedState={this.queryObject.state}/>
          }
        </div>
      </li>
    )
  }
}

export default withRouter(PageHeader)