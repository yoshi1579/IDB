import {
  states, 
  animals, 
  sortAnimals, 
  sortVets, 
  sortShelters, 
  sizes, 
  ages, 
  sexes, 
  ratings, 
  mapToDisplay,
  mapSizeToDisplay
} from './../Assets/StaticData'
import React, {Component} from 'react';
import axios from 'axios';
import queryString from 'query-string';
import {Link} from 'react-router-dom';
import { HOST } from '../Actions/type';


/*
Component for generating button items in dropdown menu besides Reset Filter button
*/
export class DropItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
    }
    this.sortType = [];
    switch(this.props.domain) {
      case '/FindPets/': this.sortType = sortAnimals; break;
      case '/FindVets/': this.sortType = sortVets; break;
      case '/FindShelters/': this.sortType = sortShelters; break;
      default: break;
    }
  }
  
  getCities() {
    axios.get('http://'+HOST+':5000/api/pet/dropdown?state='.concat(this.props.selectedState), {
          headers: { 
              'Access-Control-Allow-Origin' : '*',
              'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
          },
          responseType: 'json',
    }).then(json => this.setState({cities: json['data'].city}));
  }
  componentWillMount() {
    if (this.props.type === "city" && this.props.selectedState !== undefined)
      this.getCities();
  }

  componentDidUpdate(prevProps) {
    if (this.props.type === 'city' && this.props.selectedState !== prevProps.selectedState)
      this.getCities();
    }

  render() {
    let items = [];
    let query = queryString.parse(this.props.queries);
    let menuItems = [];
    switch (this.props.type) {
      case 'state': delete(query['city']); menuItems = states; break;
      case 'city': menuItems = this.state.cities; break;
      case 'animal': menuItems = animals; break;
      case 'sortby': menuItems = this.sortType; query['order'] = 'ascending'; break;
      case 'size': menuItems = sizes; break;
      case 'age': menuItems = ages; break;
      case 'sex': menuItems = sexes; break;
      case 'rating': menuItems = ratings; break;
      default: break;
    }
    for (let item of menuItems) {
      let displayName = "";
      switch(this.props.type) {
        case 'state': displayName = mapToDisplay[item]; break;
        case 'rating': displayName = mapToDisplay[item]; break;
        case 'sex': displayName = mapToDisplay[item]; break;
        case 'size': displayName = mapSizeToDisplay[item]; break;
        default: displayName = item;
      }
      query[this.props.type] = item;
      items.push(<Link className={"dropdown-item ".concat(this.props.type, "-item")}
                 to={this.props.domain.concat('1?', queryString.stringify(query))} 
                 key={item}>{displayName}</Link>)
    }
    return (
      <div>
        {items}
      </div>
    )
  }
}