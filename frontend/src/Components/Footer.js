import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './../CSS/Footer.css';

class Footer extends Component{
  constructor(props) {
    super(props);
    this.style = {
      backgroundColor: "#a1887f",
    }
  }
  render(){
    return (
      <div className="foot">
        <footer className="text-white default" style={this.style}>
          <div className="container">
            <p className="float-right">
              <Link to="#">Back to top</Link>
            </p>
            <p>&copy; ConnectPetsTo</p>
          </div>
        </footer>
      </div>
    )
  }
}

export default Footer
