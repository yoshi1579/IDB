import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../CSS/ShelterItems.css'

export default class shelterItems extends Component {
  render() {
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <Link className="shelterimg" to={"/Shelters/ShelterEntity/".concat(this.props.shelterData.id)}><img className="card-img-top" src={this.props.shelterData.img_url} onError={(e)=>{e.target.src="https://www.scandichotels.se/Static/img/placeholders/image-placeholder_3x2.svg"}} alt=""/></Link>
          <div className="card-body">
            <p className="card-text"><Link to={"/Shelters/ShelterEntity/".concat(this.props.shelterData.id)}>{this.props.shelterData.name}</Link></p>
            <div className="d-flex justify-content-between align-items-center">
              <h6>
                <span className="badge badge-pill badge-light">{this.props.shelterData.city}</span>
                <span className="badge badge-pill badge-light">{this.props.shelterData.phone}</span>
              </h6>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
